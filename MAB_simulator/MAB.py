import time
from typing import Dict, Callable, Optional
import Arms
from Bandit import Bandit
import random
import math


def ids_generator():
    last_arm_id = 0
    while 1:
        yield last_arm_id
        last_arm_id += 1


# TODO Should be singleton generators

global_arms_ids_generator = ids_generator()  # The only function that keeps tracking after arms ids
global_bandit_ids_generator = ids_generator()  # The only function that keeps tracking on bandits ids


class Algorithm:

    def __init__(self, name: str):
        self.name = name
        self.rounds_played = 0
        self.cumulative_reward = 0
        self.reward_list = []
        self.bandit = None

    def initialize(self, args: Dict):
        ...

    def make_N_steps(self, N, args: Dict):
        ...

    def get_info(self):
        ...


class Algo1(Algorithm):
    def __init__(self, name: str, N_explore: int, N_exploit: int):
        super().__init__(name)
        self.N_explore = N_explore
        self.N_exploit = N_exploit
        self._is_initialized = False

    def initialize(self, args: Dict):
        if self._is_initialized:
            raise Exception("Attemp to initialize algorithm {} twice".format(self.name))
        self.bandit = args["bandit"]
        self._is_initialized = True

    def make_N_steps(self, N, args: Dict):
        if not self._is_initialized:
            raise Exception("Attemp to run an un initialize algorithm {} twice".format(self.name))

        explore_steps = max(0, min(self.N_explore - self.rounds_played, N))
        exploit_steps = N - explore_steps
        exploit_steps = max(min(self.N_exploit + self.N_explore - self.rounds_played, exploit_steps), 0)
        reward = 0
        if self.N_exploit + self.N_explore < self.rounds_played + N:
            print("Warning !!! Can't perform {} steps ,only {} steps can be done .".format(N, max(0,
                                                                                                  self.N_exploit + self.N_explore - self.rounds_played)))

        def f_aux(mode):
            order = self.bandit.arms.get_arms_id(mode=mode, args={})
            reward = self.bandit.play(arms_order=order,args={})
            self.rounds_played += 1
            self.reward_list.append(reward)
            self.cumulative_reward += reward
            return reward

        for i in range(explore_steps):
            reward += f_aux(mode="random")
        for i in range(exploit_steps):
            reward += f_aux(mode="estimated_best")

        return reward


class Algo_epsilon1(Algorithm):
    def __init__(self, name):
        super().__init__(name=name)
        self.epsilon_update_function = None  # type: Optional[ Callable[[Dict],int]]
        self._is_initialized = False

    def initialize(self, args: Dict):
        if self._is_initialized:
            raise Exception("Attemp to initialize algorithm {} twice".format(self.name))
        self.epsilon_update_function = args["epsilon_update_function"]
        self.bandit = args["bandit"]  # type: Bandit
        self._is_initialized = True

    def make_N_steps(self, N, args: Dict):

        N_steps_reward = 0
        arms_number = self.bandit.arms.get_arms_number()

        for i in range(N):
            arms_id_list = []
            epsilon = self.epsilon_update_function({"round": self.rounds_played})

            arm_iterator = self.bandit.arms.arm_iterator_best_default()
            for _ in range(arms_number):
                mode = "best"
                if random.random() > epsilon:
                    mode = "random"
                arm = arm_iterator.send(mode)
                print(arm, mode)
                arms_id_list.append(arm)

            reward = self.bandit.play(arms_order=arms_id_list,args=args)
            self.rounds_played += 1
            self.reward_list.append(reward)
            N_steps_reward += reward
            self.cumulative_reward += reward
        return N_steps_reward


class Algo_epsilon2(Algorithm):

    def __init__(self, name):
        super().__init__(name=name)
        self.epsilon_update_function = None  # type: Optional[ Callable[[Dict],int]]
        self._is_initialized = False

    def initialize(self, args: Dict):
        if self._is_initialized:
            raise Exception("Attemp to initialize algorithm {} twice".format(self.name))
        self.epsilon_update_function = args["epsilon_update_function"]
        self.bandit = args["bandit"]  # type: Bandit
        self._is_initialized = True

    def make_N_steps(self, N, args: Dict):

        N_steps_reward = 0

        for i in range(N):

            epsilon = self.epsilon_update_function({"round": self.rounds_played})

            mode = "estimated_best"
            if random.random() > epsilon:
                mode = "random"
            order = self.bandit.arms.get_arms_id(mode=mode, args={})
            print(self.rounds_played, order, mode)
            reward = self.bandit.play(arms_order=order,args=args)
            self.rounds_played += 1
            self.reward_list.append(reward)
            N_steps_reward += reward
            self.cumulative_reward += reward
        return N_steps_reward

class Algo_UCB_Thompson1(Algorithm):

    def __init__(self, name):
        super().__init__(name=name)
        self._is_initialized = False
        self.alg_mode = None

    def initialize(self, args: Dict):
        if self._is_initialized:
            raise Exception("Attemp to initialize algorithm {} twice".format(self.name))
        self.bandit = args["bandit"]  # type: Bandit
        self.alg_mode = args["alg_mode"]
        self._is_initialized = True


    def make_N_steps(self, N, args: Dict):

        N_steps_reward = 0
        args.update({"round_number": self.rounds_played})
        for i in range(N):
            order = self.bandit.arms.get_arms_id(mode=self.alg_mode, args=args)
            reward = self.bandit.play(arms_order=order,args={})
            self.rounds_played += 1
            self.reward_list.append(reward)
            N_steps_reward += reward
            self.cumulative_reward += reward
        return N_steps_reward


bandit1 = Bandit()
bandit1.initialize(Arms.random_bernoulli_arm_factory, {"arms_number": 10})

bandit2 = bandit1.copy_bandit()

def eps_update1(args: Dict):
    round_num = args["round"]
    return 0.1 + round_num * 0.001


def eps_update(args: Dict):
    round_num = args["round"]
    return 1 + (math.log(round_num + 0.000001))

print(bandit1.arms.best_possible_expected_reward())
print(bandit2.arms.best_possible_expected_reward())
# alg = Algo1("trivial1", 500, 50)
# alg.initialize({"bandit": bandit1})
# alg.make_N_steps(500, dict())
# alg.make_N_steps(50, dict())
# alg2= Algo_UCB_Thompson1("UCB1",)
# alg2.initialize({"bandit": bandit2,"alg_mode": "UCB", "epsilon_update_function": eps_update1})
# alg2.make_N_steps(1000, {})
alg3 = Algo_UCB_Thompson1("UCB1",)
alg3.initialize({"bandit": bandit1,"alg_mode": "thompson", "epsilon_update_function": eps_update1})
alg3.make_N_steps(1000, {"samples_number" : 1})

print("\n\n\n\n\n\n\n=====================================================================================================")
alg3 = Algo_UCB_Thompson1("UCB1",)
alg3.initialize({"bandit": bandit2,"alg_mode": "thompson", "epsilon_update_function": eps_update1})
alg3.make_N_steps(1000, {"samples_number" : 10})

# explo = alg.reward_list[5000:]
# alg2 = Algo_epsilon2("epsilon1")
# alg2.initialize({"bandit": bandit2, "epsilon_update_function": eps_update1})
# alg2.make_N_steps(100, {})
# expl = alg2.reward_list
# explo = alg2.reward_list[-100:]
# print(bandit1.arms.get_permutation_best_default("best"))
# print(expl)
# print(sum(expl) / len(expl), sum(explo) / len(explo))
# # sum = 0
# # for g in range(1000):
# #     order = bandit1.arms.get_permutation_best_default()
# #     bandit1.set_order(order)
# #     sum += bandit1.play({})
# #
# # sum = sum / bandit1.round_number
# # print(sum,"    ",bandit1.arms.get_permutation_best_default("best"))
# print(bandit1.arms.best_possible_expected_reward())

# if __name__ == '__main__':
#
#     ar = Arms.random_bernoulli_arm_factory({"arms_number": 10})
#
#     A = Arms.BernoulliArms(ar)
#     print(A.get_permutation())
