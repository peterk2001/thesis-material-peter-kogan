from typing import Dict, Callable, List, Optional
from scipy.stats import bernoulli, uniform
from Arms import Arm, BernoulliArms

global_temp = None


def ids_generator():
    last_arm_id = 0
    while 1:
        yield last_arm_id
        last_arm_id += 1


global_bandit_ids_generator = ids_generator()


class Bandit:
    def __init__(self):
        self.bandit_id = ids_generator()
        self.__arms_list = []  # type: List[Arm]
        self.cumulative_regret = 0
        self.arms = None  # type: Optional[BernoulliArms]      # The order in which the armes are pulled if function play is called
        self.round_number = 0
        self._is_initialized = False

        # The minimal id of all the arms that this bandit has - used for simple reordering of the arms
        self.last_regret = None

    def initialize(self, arm_init_function: Callable[[Dict], List[Arm]], args: Dict):
        if self._is_initialized:
            raise Exception("Attemp to initialize bandit {} twice".format(self.bandit_id))

        # Create arms from given factory
        self.__arms_list = arm_init_function(args)

        # TODO : check if the order is correct - integers in ascending order like in range(n,m)

        self.arms = BernoulliArms(self.__arms_list)
        self._is_initialized = True

    def __update_relevant_info(self, data: Dict):

        # TODO think how I compute \" best possible round \" rewards ( to compute the regret )
        self.last_regret = self.arms.best_possible_expected_reward() - data["round_reward"]

        self.cumulative_regret += self.last_regret
        self.round_number += 1

    def play(self, arms_order: List[int], args: Dict = None):

        if not self._is_initialized:
            raise Exception("Attemp to play uninitialized bandit {}".format(self.bandit_id))

        round_reward = 0
        # Pull the arms until failure
        for arm in self.arms.get_arms(arms_order):
            arm_reward = arm.pull(args)
            if arm_reward == 0:
                break
            round_reward += arm_reward
        print(round_reward,arms_order)
        # Update relevant inner statistics/data of the bandit ( Regret etc.)
        self.__update_relevant_info({"round_reward": round_reward})
        # print(self.round_number, round_reward)
        return round_reward

    def copy_bandit(self):

        arms_list = self.arms.copy_arms()

        new_bandit = Bandit()

        new_bandit.initialize(arm_init_function=lambda args: arms_list, args={})

        return new_bandit