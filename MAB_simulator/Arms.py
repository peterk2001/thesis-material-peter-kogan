import math

from scipy.stats import bernoulli, uniform,beta
from typing import Dict, Callable, List

import copy

import random


def ids_generator():
    last_arm_id = 0
    while 1:
        yield last_arm_id
        last_arm_id += 1


# TODO Should be singleton generators

global_arms_ids_generator = ids_generator()


class Arm:
    def __init__(self, id):
        self.__arm_id = id
        self.reward_func = lambda: 0
        self._is_initialized = False
        self._pulls = 0
        self._reward = 0

    def initialize(self, reward_function: Callable[[Dict], float]):
        ...

    def pull(self, args: Dict):
        ...

    def get_arm_id(self):
        return self.__arm_id

    def get_arm_pulls(self):
        return self._pulls

    def get_arm_reward(self):
        return self._reward


class BernoulliArm(Arm):
    def __init__(self, id: int, success_prob: float):
        super().__init__(id=id)
        if not 0 <= success_prob <= 1:
            raise ValueError(
                "Arm {id} success probability should be in [0,1] , but it is {prob} ".format(id=id, prob=success_prob))
        self.inner_random_var = bernoulli(success_prob)

    def initialize(self, reward_function: Callable[[Dict], float]):
        if self._is_initialized:
            raise Exception("Attemp to initialize arm {} twice".format(self.get_arm_id()))
        self.reward_func = reward_function
        self._is_initialized = True

    def pull(self, args: Dict):
        if not self._is_initialized:
            raise Exception("Attemp to play uninitialized arm {}".format(self.get_arm_id()))
        args.update({"inner_random_var": self.inner_random_var})
        self._pulls += 1
        reward = self.reward_func(args)
        self._reward += reward
        return reward

    def get_arm_probab(self):
        return self.inner_random_var.mean()


def one_zero_bernoulli_reward(args):
    # TODO warning if there more than 1 args
    return args["inner_random_var"].rvs()


def random_bernoulli_arm_factory(args: Dict):
    arms_number = args["arms_number"]
    # Draw uniform random variable [0,1] for arms sucess probability
    arm_probabs = uniform.rvs(size=arms_number)

    # Arrange the probabilities in the descending order
    arm_probabs.sort()
    arm_probabs = arm_probabs[::-1]

    arms = [BernoulliArm(arm_id, prob) for arm_id, prob in zip(global_arms_ids_generator, arm_probabs)]

    print([(a.get_arm_id(), a.get_arm_probab()) for a in arms])
    for arm in arms:
        arm.initialize(one_zero_bernoulli_reward)

    return arms


class BernoulliArms:
    def __init__(self, arm_list: List[BernoulliArm]):

        self.__arm_list = arm_list
        self.__arm_list.sort(key=lambda arm: arm.get_arm_id())

        self.min_arm_id = self.__arm_list[
            0].get_arm_id()  # The minimal id of all the arms that this bandit has - used for simple reordering of the arms
        self.max_expeced_reward = None

    def get_arms(self, arms_order: List[int]):
        # TODO : Check if the indexes in order are correct with respect to arms we have

        # Convert the ordered list of arm ids to ordered list of corresponding indexes in self.__arms_list
        arm_indexes = [(arm_id - self.min_arm_id) for arm_id in arms_order]

        return [self.__arm_list[i] for i in arm_indexes]


    def arm_iterator_best_default(self):
        new_arm_list = self.get_arms_id(mode="estimated_best", args={})

        def iterator_best_default():
            mode = yield
            while len(new_arm_list) > 0:
                arm_index = 0
                if mode != "best":
                    arm_index = random.randint(0, len(new_arm_list) - 1)

                arm = new_arm_list.pop(arm_index)
                mode = yield arm.get_arm_id()

        iterator = iterator_best_default()
        iterator.send(None)
        return iterator

    def get_arms_number(self):
        return len(self.__arm_list)

    # def get_permutation_best_default(self, mode="default"):
    #     temp = []
    #     iterator = self.arm_iterator_best_default()
    #
    #     for i in range(len(self.__arm_list)):
    #         temp.append(iterator.send(mode))
    #
    #     return temp

    def best_possible_expected_reward(self):
        max_reward_expect = 0

        if self.max_expeced_reward:
            return self.max_expeced_reward

        new_arm_list = self.get_arms(self.get_arms_id(mode="true_best", args={}))

        for i in range(1, len(new_arm_list)):
            prob_product = 1
            for j in range(i):
                prob_product *= new_arm_list[j].get_arm_probab()

            max_reward_expect += prob_product

        self.max_expeced_reward = max_reward_expect
        return max_reward_expect

    def get_arms_id(self, mode, args : Dict):

        def UCB_value(arm : BernoulliArm):
            round_number = args["round_number"]
            arm_pulls = arm.get_arm_pulls()
            avrg_reward = arm.get_arm_reward()/arm_pulls if arm_pulls != 0 else 0
            arm_explore_component = math.sqrt(2*math.log(round_number + 1)/(arm_pulls + 1))
            return avrg_reward + arm_explore_component

        def sampled_mean(arm: BernoulliArm):
            samples_number = args["samples_number"]
            sampled_mean_avrg = 0
            arm_pulls = arm.get_arm_pulls()
            arm_reward = arm.get_arm_reward()
            arm_failed_pulls = arm_pulls - arm_reward
            for i in range(samples_number):
                sampled_mean = beta.rvs(a=arm_reward + 1,b = arm_failed_pulls + 1)
                sampled_mean_avrg += sampled_mean
            return sampled_mean_avrg/samples_number

        def random_number(arm: BernoulliArm):
            return random.random()

        def avrg_reward(arm: BernoulliArm):
            return arm.get_arm_reward() / arm.get_arm_pulls()

        def true_probab(arm: BernoulliArm):
            return arm.get_arm_probab()

        possible_modes = {"estimated_best": avrg_reward,
                          "random":random_number,
                          "true_best":true_probab,
                          "UCB":UCB_value,
                          "thompson":sampled_mean}

        value_func = possible_modes[mode]
        arm_list = [(arm.get_arm_id(),value_func(arm)) for arm in self.__arm_list]
        arm_list.sort(key=lambda tupl : tupl[1],reverse=True)
        arm_list = [arm_tuple[0] for arm_tuple in arm_list]

        h =   [(arm.get_arm_id(), arm.get_arm_pulls(), arm.get_arm_reward(),arm.get_arm_probab(), value_func(arm)) for arm in
             self.get_arms(arm_list)]
        h.sort(key=lambda t : t[4], reverse=True)
        print(h)
        return arm_list


    def copy_arms(self):

        probabilities = [arm.get_arm_probab() for arm in self.__arm_list]

        probabilities.sort(reverse=True)

        new_arms = [BernoulliArm(arm_id, prob) for arm_id, prob in zip(global_arms_ids_generator, probabilities)]

        [new_arm.initialize(old_arm.reward_func) for new_arm,old_arm in zip(new_arms,self.__arm_list)]

        return new_arms


